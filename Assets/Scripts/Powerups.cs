﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerups : MonoBehaviour {

    private GameObject weapon;
    public GameObject followers;

    Rigidbody2D rb;

    //Defines what kind of powerup the player gets
    public bool tripleType;
    public bool beamType;

    public bool followerType;

    void Start()
    {
        weapon = GameObject.FindWithTag("Weapon");
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -100);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //checks if the colliding object is player
        if (col.gameObject.tag == "Player")
        {
            if (followerType)
            {
                followers.SetActive(true);
            }

            if(tripleType)
            {
                //If the powerup is tripletype, enables the triple firing mode from playershooting script
                weapon.GetComponent<PlayerShooting>().timer = 0;
                weapon.GetComponent<PlayerShooting>().basicFireMode = false;
                weapon.GetComponent<PlayerShooting>().tripleFireMode = true;
                weapon.GetComponent<PlayerShooting>().beamFireMode = false;

            }
            else if(beamType)
            {
                //If the powerup is beamtype, enables the beam firing mode from playershooting script
                weapon.GetComponent<PlayerShooting>().timer = 0;
                weapon.GetComponent<PlayerShooting>().basicFireMode = false;
                weapon.GetComponent<PlayerShooting>().tripleFireMode = false;
                weapon.GetComponent<PlayerShooting>().beamFireMode = true;
            }
            Destroy(gameObject);
        }
    }
}
