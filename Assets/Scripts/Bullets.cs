﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets : MonoBehaviour
{
    public float speed;
    public static float damage = 10;

    private Camera mainCamera;

    private Rigidbody2D rb;

    void Start()
    {
        damage = 10;
        //If no rigidbody hasn't been defined, this sets rb as rigidbody
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }

        mainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();

        //Moves the bullet upwards
        rb.velocity = transform.up * speed;

        //Destroys the bullet in after 2 seconds, so they don't waste resources
        Destroy(gameObject, 2f);
    }

    private void Update()
    {
        if(transform.position.y > mainCamera.orthographicSize)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}