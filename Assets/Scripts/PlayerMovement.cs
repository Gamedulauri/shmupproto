﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

public class PlayerMovement : MonoBehaviour
{

    public Vector2 speed = new Vector2(250, 250);

    private Vector2 movement;
    private Rigidbody2D rb;

    public Boundary boundary;

    Animator anim;

    private void Start()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }

        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");

        movement = new Vector2(inputX * speed.x, inputY * speed.y);

        anim.SetFloat("turn", inputX);
    }

    void FixedUpdate()
    {
        rb.velocity = movement;

        rb.position = new Vector3(Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(rb.position.y, boundary.yMin, boundary.yMax),
            0f);
    }
}