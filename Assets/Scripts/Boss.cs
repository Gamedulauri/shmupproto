﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

    Rigidbody2D rb;

    public float speedX = 0f;
    public float speedY = -30f;

    public float hitPoints = 10000f;

    public static bool BossDefeated;

    public GameObject Phase1;
    public GameObject Phase2;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        BossDefeated = false;
    }

    private void Update()
    {
        Debug.Log(hitPoints);
        if(hitPoints <= 10000)
        {
            Phase2Enabled();
        }
        if (hitPoints <= 0)
        {
            UIManager.score += 1000;
            BossDefeated = true;

            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(transform.position.y >= 500)
        {
            rb.velocity = new Vector2(speedX, speedY);
        }
        else
        {
            rb.velocity = new Vector2(speedX, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)

    {
        if (coll.gameObject.tag == "PlayerBullet")
        {
            if (hitPoints > 0)
            {
                UIManager.score += 50;
                hitPoints = hitPoints - Bullets.damage;
            }
        }
    }
    void Phase2Enabled()
    {
        Phase1.SetActive(false);
        Phase2.SetActive(true);
    }
}
