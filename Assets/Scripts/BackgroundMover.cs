﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMover : MonoBehaviour {
 Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }
	}

    


    void Update()
    {
        //Checks if background's position is lower than -600 and if it is lower, then set y position to 560.
        if(transform.position.y <= -780)
        {
            transform.position = new Vector2(transform.position.x, 1140);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Moves background downwards by y axis.
        rb.velocity = new Vector2(rb.velocity.x, -50);
    }

}
