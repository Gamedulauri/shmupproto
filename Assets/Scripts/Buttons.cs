﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartButtons()
    {
        SceneManager.LoadScene(1);
    }
    public void QuitButton()
    {
        Application.Quit();
    }
    public void VictoryScreenButton()
    {
        SceneManager.LoadScene(0);
    }
    public void DeathScreenButton1()
    {
        SceneManager.LoadScene(Application.loadedLevel);
    }
    public void DeathScreenButton2()
    {
        SceneManager.LoadScene(0);
    }
}
