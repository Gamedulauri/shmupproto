﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    public GameObject EnemyBullet;

    public GameObject player;

    float nextFire;

    public float fireRate;

    public bool rotateRight;
    public bool rotateLeft;
    public bool aim;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Update () {

        if (rotateLeft)
        {
            RotateLeftFire();
        }
        else if (rotateRight)
        {
            RotateRightFire();
        }
        else if (aim)
        {
            AimingFire();
        }
        else
        {
            Fire();
        }
	}



    void Fire()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(EnemyBullet, transform.position, transform.rotation);
        }
    }



    void RotateRightFire()
    {
        transform.Rotate(0, 0, 1f);
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(EnemyBullet, transform.position, transform.rotation);
        }
    }
    void RotateLeftFire()
    {
        transform.Rotate(0, 0, -1f);
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(EnemyBullet, transform.position, transform.rotation);
        }
    }

    void AimingFire()
    {
        if(PlayerAlive.playerHitPoints >= 1)
        {
             transform.LookAt(player.transform.position);
             transform.Rotate(0, -90, 90);
        }
        else
        {
            return;
        }


        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(EnemyBullet, transform.position, transform.rotation);
        }
    }
}
