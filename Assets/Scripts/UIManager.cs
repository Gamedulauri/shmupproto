﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public GameObject VictoryPanel;
    public GameObject DeathPanel;

    float totalHp;
    float currentHp;

    GameObject hpBar;

    public static int score;

    public Text ScoreText;


	// Use this for initialization
	void Start () {
        score = 0;
        totalHp = 100;

        hpBar = GameObject.Find("HpBarForeground");
	}
	
	// Update is called once per frame
	void Update () {

        CheckBossDefeated();
        HpBar();
        ScoreUpdate();
        DeathScreen();
        BackToMainMenu();
	}

    void CheckBossDefeated()
    {
        if (Boss.BossDefeated == true)
        {
            VictoryPanel.SetActive(true);
            Time.timeScale = 0.3f;
        }
    }

    void HpBar()
    {       
        hpBar.transform.localScale = new Vector3((PlayerAlive.playerHitPoints / totalHp), 1, 1);
    }

    void ScoreUpdate()
    {
        ScoreText.text = "Score \n" + score.ToString();
    }
    void DeathScreen()
    {
        if (!GameObject.Find("PlayerShip"))
        {
            DeathPanel.SetActive(true);
        }
    }

    void BackToMainMenu()
    {
        if(Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }
}
