﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    //Different kinds of firing modes
    public bool basicFireMode;
    public bool tripleFireMode;
    public bool beamFireMode;

    public float powerupDuration = 10f;
    public float timer = 0f;

    public GameObject shot;
    public float fireRate;

    private float nextFire;


    private void Start()
    {
        basicFireMode = true;
    }

    void Update()
    {
        if(basicFireMode == true)
        {
            Fire();
        }
        else if(tripleFireMode == true)
        {
            if(timer < powerupDuration)
            {
                tripleFire();

                timer = timer + Time.deltaTime;
            }
            else
            {
                basicFireMode = true;
                tripleFireMode = false;

                timer = 0;
            }

        }
        else if (beamFireMode == true)
        {
            if (timer < powerupDuration)
            {
                BeamFire();

                timer = timer + Time.deltaTime;
            }
            else
            {
                basicFireMode = true;
                beamFireMode = false;

                timer = 0;
            }
        }
    }

    void Fire()
    {
        Bullets.damage = 10;
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            
            nextFire = Time.time + fireRate;
            Instantiate(shot, transform.position, transform.rotation);
        }
    }

    void tripleFire()
    {
        Bullets.damage = 6;
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {

            nextFire = Time.time + fireRate;
            Instantiate(shot, transform.position, transform.rotation);
            Instantiate(shot, new Vector2(transform.position.x - 10, transform.position.y), Quaternion.Euler(0, 0, -10));
            Instantiate(shot, new Vector2(transform.position.x + 10, transform.position.y), Quaternion.Euler(0, 0, 10));
        }
    }

    void BeamFire()
    {
        Bullets.damage = 1f;
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {

            nextFire = Time.time + fireRate * 0.15f;
            Instantiate(shot, transform.position, transform.rotation);
        }
    }

}
