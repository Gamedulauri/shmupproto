﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAlive : MonoBehaviour {


    SpriteRenderer playerSprite;

    public static int playerHitPoints = 100;

    private bool invinsibility = false;

	// Use this for initialization
	void Start () {
        playerSprite = GameObject.FindWithTag("PlayerSprite").GetComponent<SpriteRenderer>();
        Boss.BossDefeated = false;
        Time.timeScale = 1;
        playerHitPoints = 100;
        invinsibility = false;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(playerHitPoints);
        if(playerHitPoints <= 0)
        {
            Destroy(gameObject);
        }
        if(Boss.BossDefeated == true)
        {
            invinsibility = true;
        }

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "Enemy" || col.gameObject.tag == "EnemyBullet")
        {
            if (invinsibility == false)
            {
                if (playerHitPoints >= 1)
                {
                    playerHitPoints = playerHitPoints - 10;

                    StartCoroutine(Blinker());
                    StartCoroutine(InvinsibilityTriggerer());
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    IEnumerator Blinker()
    {
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = false;
        yield return new WaitForSeconds(0.1f);
        playerSprite.enabled = true;

        StopCoroutine(Blinker());
    }
    IEnumerator InvinsibilityTriggerer()
    {
        invinsibility = true;
        yield return new WaitForSeconds(2f);
        invinsibility = false;

        StopCoroutine(InvinsibilityTriggerer());
    }
}
