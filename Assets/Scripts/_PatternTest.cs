﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _PatternTest : MonoBehaviour {


    public GameObject EnemyBullet;

    public GameObject player;

    float nextFire;

    public float fireRate;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        Fire();
    }



    void Fire()
    {
        transform.Rotate(0, 0, 1f);
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(EnemyBullet, transform.position, transform.rotation);
            Instantiate(EnemyBullet, transform.position, new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z + 90));
        }

        //transform.LookAt(player.transform.position);
        //transform.Rotate(0, -90, 90);
    }
}
