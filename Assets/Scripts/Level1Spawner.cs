﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1Spawner : MonoBehaviour {

    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Enemy3;
    public GameObject Boss1;

    public GameObject TripleShot;
    public GameObject Beam;
    public GameObject Followers;



    // Use this for initialization
    void Start () {

        StartCoroutine(SpawnOrder());
		
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(Time.timeSinceLevelLoad);
        if(Input.GetKey(KeyCode.I))
        {
            Instantiate(Boss1, new Vector3(0, 550, 0), Quaternion.identity);
        }
    }
    

    IEnumerator SpawnOrder()
    {
        Debug.Log("instantiated enemy");
        Instantiate(Enemy1, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(250, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 700, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(125, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(4f);


        Debug.Log("instantiated enemy");
        Instantiate(Enemy1, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 750, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(125, 550, 0), Quaternion.identity);
        yield return new WaitForSeconds(2f);

        Debug.Log("instantiated powerup");
        Instantiate(TripleShot, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(2f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy2, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-250, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 800, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy1, new Vector3(0, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-250, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(250, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(125, 650, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy1, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-125, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Instantiate(Enemy1, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(250, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 700, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(125, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy2, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-250, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 800, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy3, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 650, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(0, 750, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 550, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated powerup");
        Instantiate(Beam, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(2f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy1, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-125, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(2f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy3, new Vector3(0, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-250, 650, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(250, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 650, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 650, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy2, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-250, 650, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 700, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 800, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy3, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(0, 650, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(0, 750, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(125, 550, 0), Quaternion.identity);
        yield return new WaitForSeconds(3f);

        Debug.Log("instantiated enemy");
        Instantiate(Enemy2, new Vector3(0, 550, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(-250, 550, 0), Quaternion.identity);
        Instantiate(Enemy3, new Vector3(250, 550, 0), Quaternion.identity);
        Instantiate(Enemy1, new Vector3(-125, 700, 0), Quaternion.identity);
        Instantiate(Enemy2, new Vector3(125, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(4f);

        Debug.Log("instantiated boss");
        Instantiate(Boss1, new Vector3(0, 750, 0), Quaternion.identity);
        yield return new WaitForSeconds(8f);

        Debug.Log("instantiated powerup");
        Instantiate(TripleShot, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(20f);

        Debug.Log("instantiated powerup");
        Instantiate(Beam, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(20f);

        Debug.Log("instantiated powerup");
        Instantiate(TripleShot, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(15f);

        Debug.Log("instantiated powerup");
        Instantiate(Beam, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(20f);

        Debug.Log("instantiated powerup");
        Instantiate(TripleShot, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(15f);

        Debug.Log("instantiated powerup");
        Instantiate(Beam, new Vector3(0, 700, 0), Quaternion.identity);
        yield return new WaitForSeconds(2f);

        StopCoroutine(SpawnOrder());
    }
        
}
