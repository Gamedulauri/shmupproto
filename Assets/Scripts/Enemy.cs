﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    Rigidbody2D rb;

    public float speedX = 0f;
    public float speedY = -100f;

    public float hitPoints = 20f;

	// Use this for initialization
	void Start () {
        rb = gameObject.GetComponent<Rigidbody2D>();
        Destroy(gameObject, 10f);
	}

    private void Update()
    {
        if (hitPoints <= 0)
        {
            Destroy(gameObject);
            UIManager.score += 100;
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        rb.velocity = new Vector2(speedX, speedY);
	}

    void OnTriggerEnter2D(Collider2D coll)
        
    {
        if(coll.gameObject.tag == "PlayerBullet")
        {
            if(hitPoints > 0)
            {
                UIManager.score += 50;
                hitPoints = hitPoints - Bullets.damage;
            }
        }
    }
}
